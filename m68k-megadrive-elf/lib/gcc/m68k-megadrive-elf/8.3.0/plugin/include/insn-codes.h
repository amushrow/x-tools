/* Generated automatically by the program `gencodes'
   from the machine description file `md'.  */

#ifndef GCC_INSN_CODES_H
#define GCC_INSN_CODES_H

enum insn_code {
  CODE_FOR_nothing = 0,
  CODE_FOR_pushdi = 2,
  CODE_FOR_tstdi = 3,
  CODE_FOR_tstsf_68881 = 8,
  CODE_FOR_tstdf_68881 = 9,
  CODE_FOR_tstxf_68881 = 10,
  CODE_FOR_tstsf_cf = 11,
  CODE_FOR_tstdf_cf = 12,
   CODE_FOR_tstxf_cf = CODE_FOR_nothing,
  CODE_FOR_cmpdi = 14,
  CODE_FOR_pushexthisi_const = 33,
  CODE_FOR_movsf_cf_soft = 50,
  CODE_FOR_movsf_cf_hard = 51,
  CODE_FOR_movdf_cf_soft = 53,
  CODE_FOR_movdf_cf_hard = 54,
  CODE_FOR_pushasi = 60,
  CODE_FOR_truncsiqi2 = 61,
  CODE_FOR_trunchiqi2 = 62,
  CODE_FOR_truncsihi2 = 63,
  CODE_FOR_zero_extendqidi2 = 66,
  CODE_FOR_zero_extendhidi2 = 67,
  CODE_FOR_zero_extendhisi2 = 70,
  CODE_FOR_zero_extendqisi2 = 73,
  CODE_FOR_extendqidi2 = 74,
  CODE_FOR_extendhidi2 = 75,
  CODE_FOR_extendsidi2 = 76,
  CODE_FOR_extendplussidi = 77,
  CODE_FOR_extendqihi2 = 80,
  CODE_FOR_extendsfdf2_cf = 84,
  CODE_FOR_truncdfsf2_cf = 86,
  CODE_FOR_floatsisf2_68881 = 88,
  CODE_FOR_floatsidf2_68881 = 89,
  CODE_FOR_floatsixf2_68881 = 90,
  CODE_FOR_floatsisf2_cf = 91,
  CODE_FOR_floatsidf2_cf = 92,
   CODE_FOR_floatsixf2_cf = CODE_FOR_nothing,
  CODE_FOR_floathisf2_68881 = 93,
  CODE_FOR_floathidf2_68881 = 94,
  CODE_FOR_floathixf2_68881 = 95,
  CODE_FOR_floathisf2_cf = 96,
  CODE_FOR_floathidf2_cf = 97,
   CODE_FOR_floathixf2_cf = CODE_FOR_nothing,
  CODE_FOR_floatqisf2_68881 = 98,
  CODE_FOR_floatqidf2_68881 = 99,
  CODE_FOR_floatqixf2_68881 = 100,
  CODE_FOR_floatqisf2_cf = 101,
  CODE_FOR_floatqidf2_cf = 102,
   CODE_FOR_floatqixf2_cf = CODE_FOR_nothing,
  CODE_FOR_fix_truncdfsi2 = 103,
  CODE_FOR_fix_truncdfhi2 = 104,
  CODE_FOR_fix_truncdfqi2 = 105,
  CODE_FOR_ftruncsf2_68881 = 106,
  CODE_FOR_ftruncdf2_68881 = 107,
  CODE_FOR_ftruncxf2_68881 = 108,
  CODE_FOR_ftruncsf2_cf = 109,
  CODE_FOR_ftruncdf2_cf = 110,
   CODE_FOR_ftruncxf2_cf = CODE_FOR_nothing,
  CODE_FOR_fixsfqi2_68881 = 111,
  CODE_FOR_fixdfqi2_68881 = 112,
  CODE_FOR_fixxfqi2_68881 = 113,
  CODE_FOR_fixsfqi2_cf = 114,
  CODE_FOR_fixdfqi2_cf = 115,
   CODE_FOR_fixxfqi2_cf = CODE_FOR_nothing,
  CODE_FOR_fixsfhi2_68881 = 116,
  CODE_FOR_fixdfhi2_68881 = 117,
  CODE_FOR_fixxfhi2_68881 = 118,
  CODE_FOR_fixsfhi2_cf = 119,
  CODE_FOR_fixdfhi2_cf = 120,
   CODE_FOR_fixxfhi2_cf = CODE_FOR_nothing,
  CODE_FOR_fixsfsi2_68881 = 121,
  CODE_FOR_fixdfsi2_68881 = 122,
  CODE_FOR_fixxfsi2_68881 = 123,
  CODE_FOR_fixsfsi2_cf = 124,
  CODE_FOR_fixdfsi2_cf = 125,
   CODE_FOR_fixxfsi2_cf = CODE_FOR_nothing,
  CODE_FOR_adddi_lshrdi_63 = 126,
  CODE_FOR_adddi_sexthishl32 = 127,
  CODE_FOR_adddi_dishl32 = 130,
  CODE_FOR_adddi3 = 131,
  CODE_FOR_addsi_lshrsi_31 = 132,
  CODE_FOR_addhi3 = 136,
  CODE_FOR_addqi3 = 139,
  CODE_FOR_addsf3_floatsi_68881 = 142,
  CODE_FOR_adddf3_floatsi_68881 = 143,
  CODE_FOR_addxf3_floatsi_68881 = 144,
  CODE_FOR_addsf3_floathi_68881 = 145,
  CODE_FOR_adddf3_floathi_68881 = 146,
  CODE_FOR_addxf3_floathi_68881 = 147,
  CODE_FOR_addsf3_floatqi_68881 = 148,
  CODE_FOR_adddf3_floatqi_68881 = 149,
  CODE_FOR_addxf3_floatqi_68881 = 150,
  CODE_FOR_addsf3_68881 = 151,
  CODE_FOR_adddf3_68881 = 152,
  CODE_FOR_addxf3_68881 = 153,
  CODE_FOR_addsf3_cf = 154,
  CODE_FOR_adddf3_cf = 155,
   CODE_FOR_addxf3_cf = CODE_FOR_nothing,
  CODE_FOR_subdi_sexthishl32 = 156,
  CODE_FOR_subdi_dishl32 = 157,
  CODE_FOR_subdi3 = 158,
  CODE_FOR_subsi3 = 159,
  CODE_FOR_subhi3 = 161,
  CODE_FOR_subqi3 = 163,
  CODE_FOR_subsf3_floatsi_68881 = 165,
  CODE_FOR_subdf3_floatsi_68881 = 166,
  CODE_FOR_subxf3_floatsi_68881 = 167,
  CODE_FOR_subsf3_floathi_68881 = 168,
  CODE_FOR_subdf3_floathi_68881 = 169,
  CODE_FOR_subxf3_floathi_68881 = 170,
  CODE_FOR_subsf3_floatqi_68881 = 171,
  CODE_FOR_subdf3_floatqi_68881 = 172,
  CODE_FOR_subxf3_floatqi_68881 = 173,
  CODE_FOR_subsf3_68881 = 174,
  CODE_FOR_subdf3_68881 = 175,
  CODE_FOR_subxf3_68881 = 176,
  CODE_FOR_subsf3_cf = 177,
  CODE_FOR_subdf3_cf = 178,
   CODE_FOR_subxf3_cf = CODE_FOR_nothing,
  CODE_FOR_mulhi3 = 179,
  CODE_FOR_mulhisi3 = 180,
  CODE_FOR_umulhisi3 = 184,
  CODE_FOR_const_umulsi3_highpart = 191,
  CODE_FOR_const_smulsi3_highpart = 193,
  CODE_FOR_mulsf3_floatsi_68881 = 194,
  CODE_FOR_muldf3_floatsi_68881 = 195,
  CODE_FOR_mulxf3_floatsi_68881 = 196,
  CODE_FOR_mulsf3_floathi_68881 = 197,
  CODE_FOR_muldf3_floathi_68881 = 198,
  CODE_FOR_mulxf3_floathi_68881 = 199,
  CODE_FOR_mulsf3_floatqi_68881 = 200,
  CODE_FOR_muldf3_floatqi_68881 = 201,
  CODE_FOR_mulxf3_floatqi_68881 = 202,
  CODE_FOR_muldf_68881 = 203,
  CODE_FOR_mulsf_68881 = 204,
  CODE_FOR_mulxf3_68881 = 205,
  CODE_FOR_fmulsf3_cf = 206,
  CODE_FOR_fmuldf3_cf = 207,
   CODE_FOR_fmulxf3_cf = CODE_FOR_nothing,
  CODE_FOR_divsf3_floatsi_68881 = 208,
  CODE_FOR_divdf3_floatsi_68881 = 209,
  CODE_FOR_divxf3_floatsi_68881 = 210,
  CODE_FOR_divsf3_floathi_68881 = 211,
  CODE_FOR_divdf3_floathi_68881 = 212,
  CODE_FOR_divxf3_floathi_68881 = 213,
  CODE_FOR_divsf3_floatqi_68881 = 214,
  CODE_FOR_divdf3_floatqi_68881 = 215,
  CODE_FOR_divxf3_floatqi_68881 = 216,
  CODE_FOR_divsf3_68881 = 217,
  CODE_FOR_divdf3_68881 = 218,
  CODE_FOR_divxf3_68881 = 219,
  CODE_FOR_divsf3_cf = 220,
  CODE_FOR_divdf3_cf = 221,
   CODE_FOR_divxf3_cf = CODE_FOR_nothing,
  CODE_FOR_divmodhi4 = 226,
  CODE_FOR_udivmodhi4 = 227,
  CODE_FOR_andsi3_internal = 229,
  CODE_FOR_andsi3_5200 = 230,
  CODE_FOR_andhi3 = 231,
  CODE_FOR_andqi3 = 234,
  CODE_FOR_iordi_zext = 237,
  CODE_FOR_iorsi3_internal = 238,
  CODE_FOR_iorsi3_5200 = 239,
  CODE_FOR_iorhi3 = 240,
  CODE_FOR_iorqi3 = 243,
  CODE_FOR_iorsi_zexthi_ashl16 = 246,
  CODE_FOR_iorsi_zext = 247,
  CODE_FOR_xorsi3_internal = 248,
  CODE_FOR_xorsi3_5200 = 249,
  CODE_FOR_xorhi3 = 250,
  CODE_FOR_xorqi3 = 253,
  CODE_FOR_negdi2_internal = 256,
  CODE_FOR_negdi2_5200 = 257,
  CODE_FOR_negsi2_internal = 258,
  CODE_FOR_negsi2_5200 = 259,
  CODE_FOR_neghi2 = 260,
  CODE_FOR_negqi2 = 262,
  CODE_FOR_negsf2_68881 = 264,
  CODE_FOR_negdf2_68881 = 265,
  CODE_FOR_negxf2_68881 = 266,
  CODE_FOR_negsf2_cf = 267,
  CODE_FOR_negdf2_cf = 268,
   CODE_FOR_negxf2_cf = CODE_FOR_nothing,
  CODE_FOR_sqrtsf2_68881 = 269,
  CODE_FOR_sqrtdf2_68881 = 270,
  CODE_FOR_sqrtxf2_68881 = 271,
  CODE_FOR_sqrtsf2_cf = 272,
  CODE_FOR_sqrtdf2_cf = 273,
   CODE_FOR_sqrtxf2_cf = CODE_FOR_nothing,
  CODE_FOR_abssf2_68881 = 274,
  CODE_FOR_absdf2_68881 = 275,
  CODE_FOR_absxf2_68881 = 276,
  CODE_FOR_abssf2_cf = 277,
  CODE_FOR_absdf2_cf = 278,
   CODE_FOR_absxf2_cf = CODE_FOR_nothing,
  CODE_FOR_one_cmplsi2_internal = 281,
  CODE_FOR_one_cmplsi2_5200 = 282,
  CODE_FOR_one_cmplhi2 = 283,
  CODE_FOR_one_cmplqi2 = 285,
  CODE_FOR_ashldi_extsi = 287,
  CODE_FOR_ashldi_sexthi = 288,
  CODE_FOR_ashlsi_16 = 292,
  CODE_FOR_ashlsi_17_24 = 293,
  CODE_FOR_ashlsi3 = 294,
  CODE_FOR_ashlhi3 = 295,
  CODE_FOR_ashlqi3 = 297,
  CODE_FOR_ashrsi_16 = 299,
  CODE_FOR_subreghi1ashrdi_const32 = 301,
  CODE_FOR_subregsi1ashrdi_const32 = 302,
  CODE_FOR_ashrdi_const = 306,
  CODE_FOR_ashrsi_31 = 307,
  CODE_FOR_ashrsi3 = 308,
  CODE_FOR_ashrhi3 = 309,
  CODE_FOR_ashrqi3 = 311,
  CODE_FOR_subreg1lshrdi_const32 = 313,
  CODE_FOR_lshrsi_31 = 318,
  CODE_FOR_lshrsi_16 = 319,
  CODE_FOR_lshrsi_17_24 = 320,
  CODE_FOR_lshrsi3 = 321,
  CODE_FOR_lshrhi3 = 322,
  CODE_FOR_lshrqi3 = 324,
  CODE_FOR_rotlsi_16 = 326,
  CODE_FOR_rotlsi3 = 327,
  CODE_FOR_rotlhi3 = 328,
  CODE_FOR_rotlqi3 = 330,
  CODE_FOR_rotrsi3 = 332,
  CODE_FOR_rotrhi3 = 333,
  CODE_FOR_rotrhi_lowpart = 334,
  CODE_FOR_rotrqi3 = 335,
  CODE_FOR_bsetmemqi = 337,
  CODE_FOR_bclrmemqi = 342,
  CODE_FOR_scc0_di = 363,
  CODE_FOR_scc0_di_5200 = 364,
  CODE_FOR_scc_di = 365,
  CODE_FOR_scc_di_5200 = 366,
  CODE_FOR_beq0_di = 389,
  CODE_FOR_bne0_di = 390,
  CODE_FOR_bge0_di = 391,
  CODE_FOR_blt0_di = 392,
  CODE_FOR_beq = 393,
  CODE_FOR_bne = 394,
  CODE_FOR_bgt = 395,
  CODE_FOR_bgtu = 396,
  CODE_FOR_blt = 397,
  CODE_FOR_bltu = 398,
  CODE_FOR_bge = 399,
  CODE_FOR_bgeu = 400,
  CODE_FOR_ble = 401,
  CODE_FOR_bleu = 402,
  CODE_FOR_bordered = 403,
  CODE_FOR_bunordered = 404,
  CODE_FOR_buneq = 405,
  CODE_FOR_bunge = 406,
  CODE_FOR_bungt = 407,
  CODE_FOR_bunle = 408,
  CODE_FOR_bunlt = 409,
  CODE_FOR_bltgt = 410,
  CODE_FOR_jump = 429,
  CODE_FOR_blockage = 443,
  CODE_FOR_nop = 444,
  CODE_FOR_load_got = 452,
  CODE_FOR_indirect_jump = 453,
  CODE_FOR_extendsfxf2 = 455,
  CODE_FOR_extenddfxf2 = 456,
  CODE_FOR_truncxfdf2 = 457,
  CODE_FOR_truncxfsf2 = 458,
  CODE_FOR_sinsf2 = 459,
  CODE_FOR_sindf2 = 460,
  CODE_FOR_sinxf2 = 461,
  CODE_FOR_cossf2 = 462,
  CODE_FOR_cosdf2 = 463,
  CODE_FOR_cosxf2 = 464,
  CODE_FOR_trap = 465,
  CODE_FOR_stack_tie = 467,
  CODE_FOR_ib = 468,
  CODE_FOR_atomic_compare_and_swapqi_1 = 469,
  CODE_FOR_atomic_compare_and_swaphi_1 = 470,
  CODE_FOR_atomic_compare_and_swapsi_1 = 471,
  CODE_FOR_atomic_test_and_set_1 = 472,
  CODE_FOR_cbranchdi4 = 473,
  CODE_FOR_cstoredi4 = 474,
  CODE_FOR_cbranchsi4 = 475,
  CODE_FOR_cstoresi4 = 476,
  CODE_FOR_cbranchhi4 = 477,
  CODE_FOR_cstorehi4 = 478,
  CODE_FOR_cbranchqi4 = 479,
  CODE_FOR_cstoreqi4 = 480,
  CODE_FOR_cbranchsf4 = 481,
  CODE_FOR_cbranchdf4 = 482,
  CODE_FOR_cbranchxf4 = 483,
  CODE_FOR_cstoresf4 = 484,
  CODE_FOR_cstoredf4 = 485,
  CODE_FOR_cstorexf4 = 486,
  CODE_FOR_movsi = 487,
  CODE_FOR_movhi = 488,
  CODE_FOR_movstricthi = 489,
  CODE_FOR_movqi = 490,
  CODE_FOR_movstrictqi = 491,
  CODE_FOR_pushqi1 = 492,
  CODE_FOR_reload_insf = 493,
  CODE_FOR_reload_outsf = 494,
  CODE_FOR_movsf = 495,
  CODE_FOR_reload_indf = 496,
  CODE_FOR_reload_outdf = 497,
  CODE_FOR_movdf = 498,
  CODE_FOR_movxf = 499,
  CODE_FOR_movdi = 500,
  CODE_FOR_zero_extendsidi2 = 501,
  CODE_FOR_zero_extendqihi2 = 502,
  CODE_FOR_extendhisi2 = 503,
  CODE_FOR_extendqisi2 = 504,
  CODE_FOR_extendsfdf2 = 505,
  CODE_FOR_truncdfsf2 = 506,
  CODE_FOR_floatsisf2 = 507,
  CODE_FOR_floatsidf2 = 508,
  CODE_FOR_floatsixf2 = 509,
  CODE_FOR_floathisf2 = 510,
  CODE_FOR_floathidf2 = 511,
  CODE_FOR_floathixf2 = 512,
  CODE_FOR_floatqisf2 = 513,
  CODE_FOR_floatqidf2 = 514,
  CODE_FOR_floatqixf2 = 515,
  CODE_FOR_ftruncsf2 = 516,
  CODE_FOR_ftruncdf2 = 517,
  CODE_FOR_ftruncxf2 = 518,
  CODE_FOR_fixsfqi2 = 519,
  CODE_FOR_fixdfqi2 = 520,
  CODE_FOR_fixxfqi2 = 521,
  CODE_FOR_fixsfhi2 = 522,
  CODE_FOR_fixdfhi2 = 523,
  CODE_FOR_fixxfhi2 = 524,
  CODE_FOR_fixsfsi2 = 525,
  CODE_FOR_fixdfsi2 = 526,
  CODE_FOR_fixxfsi2 = 527,
  CODE_FOR_addsi3 = 528,
  CODE_FOR_addsf3 = 529,
  CODE_FOR_adddf3 = 530,
  CODE_FOR_addxf3 = 531,
  CODE_FOR_subsf3 = 532,
  CODE_FOR_subdf3 = 533,
  CODE_FOR_subxf3 = 534,
  CODE_FOR_mulsi3 = 535,
  CODE_FOR_umulsidi3 = 536,
  CODE_FOR_mulsidi3 = 537,
  CODE_FOR_umulsi3_highpart = 538,
  CODE_FOR_smulsi3_highpart = 539,
  CODE_FOR_mulsf3 = 540,
  CODE_FOR_muldf3 = 541,
  CODE_FOR_mulxf3 = 542,
  CODE_FOR_divsf3 = 543,
  CODE_FOR_divdf3 = 544,
  CODE_FOR_divxf3 = 545,
  CODE_FOR_divmodsi4 = 546,
  CODE_FOR_udivmodsi4 = 547,
  CODE_FOR_andsi3 = 548,
  CODE_FOR_iorsi3 = 549,
  CODE_FOR_xorsi3 = 550,
  CODE_FOR_negdi2 = 551,
  CODE_FOR_negsi2 = 552,
  CODE_FOR_negsf2 = 553,
  CODE_FOR_negdf2 = 554,
  CODE_FOR_negxf2 = 555,
  CODE_FOR_sqrtsf2 = 556,
  CODE_FOR_sqrtdf2 = 557,
  CODE_FOR_sqrtxf2 = 558,
  CODE_FOR_abssf2 = 559,
  CODE_FOR_absdf2 = 560,
  CODE_FOR_absxf2 = 561,
  CODE_FOR_clzsi2 = 562,
  CODE_FOR_one_cmplsi2 = 563,
  CODE_FOR_ashldi3 = 564,
  CODE_FOR_ashrdi3 = 565,
  CODE_FOR_lshrdi3 = 566,
  CODE_FOR_bswapsi2 = 567,
  CODE_FOR_extv = 568,
  CODE_FOR_extzv = 569,
  CODE_FOR_insv = 570,
  CODE_FOR_tablejump = 571,
  CODE_FOR_decrement_and_branch_until_zero = 572,
  CODE_FOR_sibcall = 573,
  CODE_FOR_sibcall_value = 574,
  CODE_FOR_call = 575,
  CODE_FOR_call_value = 576,
  CODE_FOR_untyped_call = 577,
  CODE_FOR_prologue = 578,
  CODE_FOR_epilogue = 579,
  CODE_FOR_sibcall_epilogue = 580,
  CODE_FOR_return = 581,
  CODE_FOR_link = 582,
  CODE_FOR_unlink = 583,
  CODE_FOR_ctrapdi4 = 588,
  CODE_FOR_ctrapsi4 = 589,
  CODE_FOR_ctraphi4 = 590,
  CODE_FOR_ctrapqi4 = 591,
  CODE_FOR_atomic_compare_and_swapqi = 592,
  CODE_FOR_atomic_compare_and_swaphi = 593,
  CODE_FOR_atomic_compare_and_swapsi = 594,
  CODE_FOR_atomic_test_and_set = 595
};

const unsigned int NUM_INSN_CODES = 596;
#endif /* GCC_INSN_CODES_H */
