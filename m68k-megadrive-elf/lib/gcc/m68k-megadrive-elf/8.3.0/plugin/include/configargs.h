/* Generated automatically. */
static const char configuration_arguments[] = "/home/antho/crosstool-ng/build/.build/m68k-megadrive-elf/src/gcc/configure --build=x86_64-build_pc-cygwin --host=x86_64-build_pc-cygwin --target=m68k-megadrive-elf --prefix=/home/antho/x-tools/m68k-megadrive-elf --with-local-prefix=/home/antho/x-tools/m68k-megadrive-elf/m68k-megadrive-elf --with-headers=/home/antho/x-tools/m68k-megadrive-elf/m68k-megadrive-elf/include --with-newlib --enable-threads=no --disable-shared --with-cpu=68000 --with-pkgversion='crosstool-NG 1.24.0' --disable-__cxa_atexit --disable-libgomp --disable-libmudflap --disable-libmpx --disable-libssp --disable-libquadmath --disable-libquadmath-support --with-gmp=/home/antho/crosstool-ng/build/.build/m68k-megadrive-elf/buildtools --with-mpfr=/home/antho/crosstool-ng/build/.build/m68k-megadrive-elf/buildtools --with-mpc=/home/antho/crosstool-ng/build/.build/m68k-megadrive-elf/buildtools --with-isl=no --with-cloog=no --enable-lto --with-host-libstdcxx='-static-libgcc -Wl,-Bstatic,-lstdc++,-Bdynamic -lm' --enable-target-optspace --disable-nls --disable-multilib --enable-languages=c";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "mcpu=68000" } };
